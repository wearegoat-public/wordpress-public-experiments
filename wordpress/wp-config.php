<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'goat-playground-1' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'h 1wd$;PsCTmN|IZSsh<&nU?NxR.-W{3gY6?Uc62s795KArXyumJPZ-0~2O#SKK$' );
define( 'SECURE_AUTH_KEY',  '/>Nn#@~>j#g7?+sEpaQSXP|(ov$8pj[5lmHJ6 8-b;se:Fz8m|N#{wg0H1:pd{N4' );
define( 'LOGGED_IN_KEY',    'gC,Ef=hh`Rf/iW )tJ]?I|Y>S)+K%8dB2?t>_fIL>7v[B)_c+_o}i?<E]ulM|K l' );
define( 'NONCE_KEY',        '3M<XoKfT-ti~v-K?K_wT?E%cV.3l~*5^/v/HTmp%x>VW4El!NXZbAIbev5S(=:z.' );
define( 'AUTH_SALT',        'zbP3_l*B/5y@[;! QD 0oU>xb?enkT,cLh5]Fd K<s_RPOG,s[*Jmd5e5l%s3Vbd' );
define( 'SECURE_AUTH_SALT', 'Y-F|tM8NZ.O RfnZ;2^o5t=<IeTV+ak@c%~BF#hkJmyr?Up&(?.}^grE/LmOGRNH' );
define( 'LOGGED_IN_SALT',   '3J]_!6fT SC}ITV.mqj[cosB=`&GpM.z7R|o./dMo8Xq6jOW@oPZ[H%1fk2#Jzhj' );
define( 'NONCE_SALT',       ' @{B&H7rxhw^%+aHFD!#LVm(7~Zc2(zg^MQ2Tw &HQP##%(i4h4~rKX>i=cA!7f%' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
